#
# Witekio docker image to build yocto projects
#
# Based on official 'ubuntu:14.04' image
#

FROM ubuntu:14.04
MAINTAINER Simon SCHIELE <sschiele@witekio.com>

# behave!
WORKDIR /tmp
ENV DEBIAN_FRONTEND noninteractive

# /bin/sh points to Dash by default, reconfigure to use bash
RUN echo "dash dash/sh boolean false" | debconf-set-selections && \
    dpkg-reconfigure -p critical dash

# Install dependencies and cleanup afterwards
RUN apt-get update && \
    apt-get install -y \
        libx11-6 libxext6 libxi6 libxt6 libxtst6 libxcb1 libx11-data libice6 \
        libsm6 x11-common libxau6 libxdmcp6 build-essential curl zip unzip \
        git-core gnupg flex bison gperf zlib1g-dev gcc-multilib g++-multilib \
        libc6-dev-i386 lib32ncurses5-dev x11proto-core-dev libx11-dev python \
        lib32z-dev ccache libgl1-mesa-dev libxml2-utils xsltproc realpath \
        bc squashfs-tools dosfstools mtools locales gawk wget git-core \
        diffstat chrpath socat cpio python python3 python3-pip sudo \
        python3-pexpect xz-utils debianutils iputils-ping unzip texinfo \
        gcc-multilib curl dosfstools mtools parted syslinux tree \
        libsdl1.2-dev liblzo2-2 liblzo2-dev uuid uuid-dev zlib1g-dev \
        liblz-dev libusb-1.0-0 libusb-1.0-0-dev gawk wget git-core diffstat unzip texinfo gcc-multilib \
        build-essential chrpath socat libsdl1.2-dev libsdl1.2-dev \
        xterm sed cvs subversion coreutils texi2html docbook-utils \
        python-pysqlite2 help2man make gcc g++ desktop-file-utils \
        libgl1-mesa-dev libglu1-mesa-dev mercurial autoconf automake \
        groff curl lzop asciidoc u-boot-tools

# i386 stuff
RUN dpkg --add-architecture i386
RUN apt-get update && \
    apt-get install -y \
        liblzo2-dev:i386 gcc-4.9-base:i386 libc6:i386 libc6-dev:i386 \
        libgcc1:i386 liblzo2-2:i386 liblzo2-dev:i386 linux-libc-dev:i386 \
        build-essential:amd64 cpp:amd64 g++:amd64 g++-multilib:amd64 \
        gcc:amd64 gcc-multilib:amd64 uuid-dev:i386 libusb-1.0-0:i386 \
        libusb-1.0-0-dev:i386

# Cleanup apt
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Add googles repo tool
RUN curl http://storage.googleapis.com/git-repo-downloads/repo > /usr/local/bin/repo
RUN chmod a+x /usr/local/bin/repo

# Add build user
RUN id build 2>/dev/null || useradd --uid 1000 --create-home build
RUN echo "build ALL=(ALL) NOPASSWD: ALL" | tee -a /etc/sudoers

# setup utf8 locales
RUN locale-gen en_US.UTF-8
RUN echo 'en_US.UTF-8 UTF-8' > /etc/locale.gen && \
    echo 'LANG="en_US.UTF-8"' > /etc/default/locale && \
    dpkg-reconfigure --frontend=noninteractive locales && \
    update-locale LANG=en_US.UTF-8
ENV LC_ALL=en_US.UTF-8
ENV LANG=en_US.UTF-8
ENV LANGUAGE=en_US.UTF-8
ENV PYTHONIOENCODING=utf-8

# Custom volumes
# VOLUME ["/tmp/ccache", "/ccache"]

# Copy ssh keys
RUN mkdir -p /home/build/.ssh
COPY deploy_keys/id_rsa /home/build/.ssh/id_rsa
COPY deploy_keys/id_rsa.pub /home/build/.ssh/id_rsa.pub
COPY deploy_keys/known_hosts /home/build/.ssh/known_hosts
RUN chown -R build: /home/build/.ssh


# Setup container init
COPY container_entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

# Setup env and run entrypoint
USER build
ENV TMPDIR="/work/hach/build"
WORKDIR /work/hach
CMD ["/entrypoint.sh"]
