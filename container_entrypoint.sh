#!/bin/bash
#
# entrypoint/init for AOSP build container
#

LANG=en_US.UTF-8

source export

bitbake console-seacloud-image
bbres=$?

if ! [[ $bbres -eq 0 ]] ; then
    echo "Error while building Image" >&2
fi

exit $bbres
