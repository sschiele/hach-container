# Build container for yocto hach customer project

This docker container is used at witekio to work on the project. If you plan on
using the docker container in a different scenario, please have a look at the
entrypoint script and ensure that used Paths are correct inside the Dockerfile.

For more details on docker and/or LXC, please refer to the public available
documentations:

 * https://docs.docker.com/
 * https://linuxcontainers.org/lxc/introduction/


## Build docker container

```
$ sudo apt-get install docker.io
$ cd container/
$ make
```


## Start build via container

The --dns flags should be only used in witekios internal network

```
$ sudo docker run \
        -v /home/simon/repos:/work \
        -it "witekio-build-yocto-hach:latest"
```


## Jump into container (for debug + dev) 

```
$ sudo docker run \
        -v /home/simon/repos:/work \
        -it "witekio-build-yocto-hach:latest" \
        /bin/bash
```


## Using container inside witekio network

To use in witekios internal network, please append flags to overwrite dns
servers inside the container:

```
$ sudo docker run \
        -v /home/simon/repos:/work \
        --dns=10.101.0.11 \
        --dns=10.26.1.1 \
        -it "witekio-build-yocto-hach:latest"
```
