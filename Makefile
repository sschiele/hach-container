SUDO = sudo
DOCKER = docker
IMAGE = witekio-build-yocto-hach

buildcontainer: Dockerfile
	$(SUDO) $(DOCKER) build -t $(IMAGE) .

all: buildcontainer

.PHONY: all
